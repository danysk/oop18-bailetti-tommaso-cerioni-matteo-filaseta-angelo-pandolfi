package it.unibo.modularcheckers;

import it.unibo.modularcheckers.controller.StartController;
import it.unibo.modularcheckers.util.LocalFilesUtils;

/**
 * Starts the Application.
 */
final class App {

    private App() {
    }

    public static void main(final String[] args) {
        LocalFilesUtils.checkDirStructure();
        new StartController();
    }
}
