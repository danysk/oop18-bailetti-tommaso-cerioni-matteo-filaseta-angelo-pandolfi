package it.unibo.modularcheckers.controller;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;
import javax.swing.Timer;

import com.google.common.base.Joiner;

import it.unibo.modularcheckers.checkers.model.engine.CheckersEngine;
import it.unibo.modularcheckers.model.Block;
import it.unibo.modularcheckers.model.BotPlayer;
import it.unibo.modularcheckers.model.Coordinate;
import it.unibo.modularcheckers.model.GameType;
import it.unibo.modularcheckers.model.Pair;
import it.unibo.modularcheckers.model.Player;
import it.unibo.modularcheckers.model.engine.Engine;
import it.unibo.modularcheckers.model.move.Step;
import it.unibo.modularcheckers.model.move.Tree;
import it.unibo.modularcheckers.view.GameTableView;
import it.unibo.modularcheckers.view.GameTableViewImpl;
import it.unibo.modularcheckers.view.observers.GameTableViewObservable;

/**
 * Basic implementation of GameLoop. This class is the implementation of the
 * controller part in the MVC pattern.
 */
public class GameLoopImpl implements GameLoop, GameTableViewObservable {

    private final Engine engine;
    private final GameTableView view;

    private static final int BOT_DELAY = 500; // milliseconds

    private Optional<Coordinate> pieceSelected;

    /**
     * Create the GameLoop and choose the engine to use.
     *
     * @param players the Player list, used to create the engine.
     * @param game    the game to be played.
     * @throws IOException if the deserialization fails.
     */
    public GameLoopImpl(final List<Player> players, final GameType game) throws IOException {
        switch (game) {
        case CHECKERS:
            this.engine = new CheckersEngine(players);
            break;
        case CHESS:
            throw new IllegalArgumentException("Function not yet implemented.");
        default:
            throw new IllegalArgumentException("Unknown Game.");
        }
        this.pieceSelected = Optional.empty();
        view = new GameTableViewImpl(this);
    }

    /**
     * @return the Engine of the GameLoop.
     */
    private Engine getEngine() {
        return engine;
    }

    /**
     * Start the gameLoop.
     */
    @Override
    public void startLoop() {
        getEngine().start();
        if (getEngine().getActualPlayer() instanceof BotPlayer) {
            botTurn();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void surrender() {
        final int res = view.surrenderDialog(
                "Player " + getEngine().getActualPlayer().getName() + " are you sure to surrender?", "Surrender?");
        if (res == JOptionPane.YES_OPTION) {
            getEngine().surrender();
            showWinnersAndRestart();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectPiece(final Optional<Coordinate> selectedPiece) {
        if (getEngine().isMoveInProgress()
                && !getEngine().getCoordinateOfLastPieceMoved().get().equals(selectedPiece.get())) {
            return;
        }
        this.pieceSelected = selectedPiece;
        if (selectedPiece.isPresent() && this.engine.getStepTreeFromCoord(selectedPiece.get()).isPresent()) {
            view.setNextMoves(this.engine.getStepTreeFromCoord(selectedPiece.get()).get().getFirstChildren().stream()
                    .map(s -> s.getCoordinate()).collect(Collectors.toList()));
        } else {
            view.setNextMoves(Collections.emptyList());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void makeStepChosen(final Coordinate whereToMove) {
        // Check if the piece was previously selected.
        if (!this.pieceSelected.isPresent()) {
            throw new IllegalStateException("No piece selected!");
        }
        Tree<Step> selectedPieceStepTree = this.getEngine().getStepTreeFromCoord(pieceSelected.get()).get();
        final Optional<Step> stepFound = selectedPieceStepTree.getFirstChildren().stream()
                .filter(s -> s.getCoordinate().equals(whereToMove)).findFirst();
        if (!stepFound.isPresent()) {
            throw new NoSuchElementException("No steps available for the coordinate: " + whereToMove.toString());
        }
        // Execute the step StepFound.
        final List<Pair<Coordinate, Block>> changes = getEngine()
                .executeStep(new Pair<>(selectedPieceStepTree.getRoot(), stepFound.get()));
        selectedPieceStepTree = selectedPieceStepTree.getChildren().stream()
                .filter(t -> t.getRoot().getCoordinate().equals(whereToMove)).findFirst().get();
        // change turn if the same piece cannot move again.
        if (selectedPieceStepTree.getChildren().isEmpty()) {
            getEngine().moveFinished();
        }
        view.applyChanges(changes);
        if (!getEngine().getWinners().isEmpty()) {
            getEngine().saveMatchToFile();
            showWinnersAndRestart();
        } else if (getEngine().getActualPlayer() instanceof BotPlayer) {
            botTurn();
        }
    }

    /**
     * Bot Player actions.
     */
    private void botTurn() {
        final BotPlayer bot = (BotPlayer) getEngine().getActualPlayer();
        final Optional<Coordinate> selectedCoord = bot.selectedPiece(getEngine().getLogicBoard(),
                getEngine().getCoordsOfMovablePieces());
        final Timer t = new Timer(BOT_DELAY, (e) -> {
            selectPiece(selectedCoord);
        });
        t.setRepeats(false);
        t.start();
        final Timer t2 = new Timer(BOT_DELAY * 2, (e) -> {
            makeStepChosen(bot.chooseStep(getEngine().getLogicBoard(),
                    getEngine().getStepTreeFromCoord(selectedCoord.get()).get().getFirstChildren()));
        });
        t2.setRepeats(false);
        t2.start();
    }

    /**
     * Shows the winners and restart the StartController.
     */
    private void showWinnersAndRestart() {
        view.winnerDialog("Player "
                + Joiner.on(",")
                        .join(getEngine().getWinners().stream().map(Player::getName).collect(Collectors.toList()))
                + " wins!");
        new StartController();
    }

}
