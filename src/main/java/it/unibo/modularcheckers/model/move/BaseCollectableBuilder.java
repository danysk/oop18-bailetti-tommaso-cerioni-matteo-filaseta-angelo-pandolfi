package it.unibo.modularcheckers.model.move;

/**
 * Interface containing basic methods of every builder for collectable classes.
 * Every new builder for collectable classes must implements this interface.
 * 
 * @param <X> the generic value of the factory.
 */
public interface BaseCollectableBuilder<X> {

    /**
     * Add a value to the builder.
     * 
     * @param value the value to add.
     */
    void addValue(X value);

    /**
     * Removes the last value inserted.
     * 
     * @return the value removed.
     */
    X removeValue();

}
