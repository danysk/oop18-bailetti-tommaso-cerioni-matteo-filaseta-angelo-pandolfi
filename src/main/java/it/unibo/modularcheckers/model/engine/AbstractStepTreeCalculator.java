package it.unibo.modularcheckers.model.engine;

import com.google.common.base.Optional;

import it.unibo.modularcheckers.model.Block;
import it.unibo.modularcheckers.model.Chessboard;
import it.unibo.modularcheckers.model.Coordinate;
import it.unibo.modularcheckers.model.move.Step;
import it.unibo.modularcheckers.model.move.Tree;
import it.unibo.modularcheckers.model.piece.Piece;

/**
 * StepTreeCalculator abstract implementation.
 */
public abstract class AbstractStepTreeCalculator implements StepTreeCalculator {
    private Chessboard chessboard;
    private Block block;
    private Piece piece;
    private Coordinate coordinate;

    /**
     * Convert the relative moveset to the absolute Tree of step.
     * 
     * @return the absolute Tree of step
     */
    protected abstract Tree<Step> generateTreeStep();

    /**
     * {@inheritDoc}
     */
    @Override
    public final Tree<Step> calc(final Chessboard chessboard, final Coordinate coordinate) {
        this.chessboard = chessboard;
        this.coordinate = coordinate;
        this.block = chessboard.getBlock(coordinate);

        final Optional<Piece> optionalPiece = this.block.getPiece();
        if (!optionalPiece.isPresent()) {
            throw new IllegalStateException();
        }
        this.piece = optionalPiece.get();

        return this.generateTreeStep();
    }

    /**
     * Get the chessboard.
     * 
     * @return the chessboard
     */
    protected Chessboard getChessboard() {
        return this.chessboard;
    }

    /**
     * Get the selected block.
     * 
     * @return the selected block
     */
    protected Block getBlock() {
        return this.block;
    }

    /**
     * Get the piece over the block.
     * 
     * @return the piece over the block
     */
    protected Piece getPiece() {
        return this.piece;
    }

    /**
     * Get the coordinate of the block.
     * 
     * @return the coordinate of the block
     */
    protected Coordinate getCoordinate() {
        return this.coordinate;
    }
}
