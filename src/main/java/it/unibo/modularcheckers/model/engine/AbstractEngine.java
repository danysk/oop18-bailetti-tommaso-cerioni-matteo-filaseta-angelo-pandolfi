package it.unibo.modularcheckers.model.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.base.Optional;

import it.unibo.modularcheckers.model.Block;
import it.unibo.modularcheckers.model.Chessboard;
import it.unibo.modularcheckers.model.Color;
import it.unibo.modularcheckers.model.Coordinate;
import it.unibo.modularcheckers.model.History;
import it.unibo.modularcheckers.model.Pair;
import it.unibo.modularcheckers.model.Player;
import it.unibo.modularcheckers.model.TurnIterator;
import it.unibo.modularcheckers.model.TurnIteratorSequential;
import it.unibo.modularcheckers.model.move.MoveBuilder;
import it.unibo.modularcheckers.model.move.MoveBuilderImpl;
import it.unibo.modularcheckers.model.move.Step;
import it.unibo.modularcheckers.model.move.Tree;
import it.unibo.modularcheckers.model.piece.Piece;

/**
 * The abstract Engine can be sub-classed to create Engines with different
 * rule-set.
 *
 */

public abstract class AbstractEngine implements Engine {
    /** The board situation. */
    private final Chessboard logicBoard;
    /** The history of the game. */
    private final History history;
    /** Every color is associated to a Player. */
    private final Map<Color, Player> players;
    /** Player that are alive during the game. */
    private final Set<Player> alivePlayers;
    /** Pieces that are NOT on board for each player. */
    private final Map<Color, List<Piece>> piecesRemoved;
    /** The turns sequence. */
    private final TurnIterator turnSequence;
    /** Move Builder to insert moves in History. */
    private final MoveBuilder moveBuilder;
    /**
     * Contains the Player that won, the players that tied, or nothing is game is
     * still in progress.
     */
    private final Set<Player> winners;
    /**
     * Contains all the trees of steps that every piece of the color in turn can do.
     */
    private Map<Coordinate, Tree<Step>> allStepTrees;
    /** Status of the Game. */
    private GameStatus status;
    /** Status of the move. */
    private boolean moveInProgress;
    /** The coordinate of the last piece moved. */
    private Optional<Coordinate> coordinateOfLastPieceMoved;

    /**
     * Sole constructor. (For invocation by subclass constructors, typically
     * implicit.)
     *
     * @param logicBoard the board configuration.
     * @param colorList  The ordered list of colors.
     * @param playerList The ordered list of players.
     */
    public AbstractEngine(final Chessboard logicBoard, final List<Color> colorList, final List<Player> playerList) {
        if (colorList.size() != playerList.size() || colorList.size() <= 1) {
            throw new IllegalArgumentException(" Wrong playerList or/and colorList size.");
        }
        this.logicBoard = logicBoard;
        this.history = new History();
        this.players = IntStream.range(0, colorList.size()).boxed()
                .collect(Collectors.toMap(i -> colorList.get(i), i -> playerList.get(i)));
        this.alivePlayers = playerList.stream().collect(Collectors.toSet());
        this.piecesRemoved = colorList.stream().collect(Collectors.toMap(c -> c, c -> new ArrayList<>()));
        this.turnSequence = new TurnIteratorSequential(colorList);
        this.moveBuilder = new MoveBuilderImpl();
        this.winners = new HashSet<>();
        this.setStatus(GameStatus.NOT_IN_PROGRESS);
        this.allStepTrees = new HashMap<>();
        this.moveInProgress = false;
        this.coordinateOfLastPieceMoved = Optional.absent();

    }

    /**
     * {@inheritDoc}
     */
    public boolean isMoveInProgress() {
        return moveInProgress;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final Player getActualPlayer() {
        return getPlayers().get(getActualTurn());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Chessboard getLogicBoard() {
        return logicBoard;
    }

    /**
     * This method is called to know who's turn is it.
     *
     * @return The Color that has to do the move in the turn in which this method is
     *         called.
     */
    public final Color getActualTurn() {
        return this.turnSequence.getActualTurn();
    }

    /**
     * {@inheritDoc}
     */
    public final GameStatus getStatus() {
        return status;
    }

    /**
     * {@inheritDoc}
     */
    public Set<Player> getWinners() {
        return winners;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final Map<Coordinate, Tree<Step>> calculateAllSteps() {
        Map<Coordinate, Tree<Step>> completeSteps = getLogicBoard().getBlocks().keySet().stream()
                .filter(c -> getLogicBoard().getBlocks().get(c).getPiece().isPresent())
                .filter(c -> getLogicBoard().getBlocks().get(c).getPiece().get().getColor().equals(getActualTurn()))
                .collect(Collectors.toMap(c -> c, c -> calculateTreeFromCoordinate(c)));
        completeSteps = filterSteps(completeSteps);
        this.allStepTrees = completeSteps;
        return Collections.unmodifiableMap(this.allStepTrees);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<Tree<Step>> getStepTreeFromCoord(final Coordinate coordSelected) {
        if (this.allStepTrees.containsKey(coordSelected)) {
            return Optional.of(this.allStepTrees.get(coordSelected));
        }
        return Optional.absent();
    }

    /**
     * {@inheritDoc}
     */
    public Set<Coordinate> getCoordsOfMovablePieces() {
        return Collections.unmodifiableSet(this.allStepTrees.keySet());
    }

    /**
     * {@inheritDoc}
     */
    public Optional<Coordinate> getCoordinateOfLastPieceMoved() {
        return this.coordinateOfLastPieceMoved;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Pair<Coordinate, Block>> executeStep(final Pair<Step, Step> steps) {
        final Coordinate oldCoord = steps.getX().getCoordinate();
        final Coordinate endCoord = steps.getY().getCoordinate();
        // Put the coordinates that could change in the collection to return.
        final List<Pair<Coordinate, Block>> changes = new ArrayList<Pair<Coordinate, Block>>();
        changes.add(new Pair<>(oldCoord, getLogicBoard().getBlock(oldCoord)));
        changes.add(new Pair<>(endCoord, getLogicBoard().getBlock(endCoord)));
        if (steps.getX().getDeadPiece().isPresent()) {
            changes.add(new Pair<>(steps.getX().getDeadPiece().get().getX(),
                    getLogicBoard().getBlock(steps.getX().getDeadPiece().get().getX())));
        }
        if (steps.getY().getDeadPiece().isPresent()) {
            changes.add(new Pair<>(steps.getY().getDeadPiece().get().getX(),
                    getLogicBoard().getBlock(steps.getY().getDeadPiece().get().getX())));
        }
        // Kill the pieces.
        killPiece(steps.getX());
        checkIfSomeoneDied(killPiece(steps.getY()));
        // Move the piece.
        final Piece piecetoMove = getLogicBoard().getBlock(oldCoord).removePiece().get();
        getLogicBoard().getBlock(endCoord).setPiece(piecetoMove);
        this.coordinateOfLastPieceMoved = Optional.of(endCoord);
        // Replace pieces if needed.
        replacePieces();
        // Add the two steps to the move builder.
        getMoveBuilder().addValue(steps.getX());
        getMoveBuilder().addValue(steps.getY());
        if (!getStepTreeFromCoord(oldCoord).get().getChildren().isEmpty()) {
            // The move is now in progress.
            setMoveInProgress(true);
            // Delete all the trees of steps but the one
            this.allStepTrees.put(endCoord, getStepTreeFromCoord(oldCoord).get().getAllNodes().stream()
                    .filter(t -> t.getRoot().getCoordinate().equals(endCoord)).findFirst().get());
            this.allStepTrees.entrySet().removeIf(e -> !e.getKey().equals(endCoord));
        }
        return changes;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveFinished() {
        setMoveInProgress(false);
        getHistory().setMove(getMoveBuilder().returnMove());
        getMoveBuilder().reset();
        nextTurn();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void start() {
        this.setStatus(GameStatus.IN_PROGRESS);
        calculateAllSteps();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void surrender() {
        this.killPlayer(this.getActualTurn());
        this.nextTurn();
    }

    /**
     * {@inheritDoc}
     */
    public void saveMatchToFile() {
        getHistory().saveToFile();
    }

    /**
     * PROTECTED METHODS.
     */

    /**
     * Called at the end of every Step made. Does additional instruction if a piece
     * died during the step.
     *
     * @param someoneDied true if a piece died during the step, false otherwise.
     */
    protected abstract void checkIfSomeoneDied(boolean someoneDied);

    /**
     * Check if the game ended, turn by turn, using the strategy pattern.
     *
     * @return An Empty Set if there's no winner yet, a set with only one Player if
     *         someone won, or a Set with more Players if there was a Tie.
     */
    protected abstract Set<Player> winCondition();

    /**
     * Checks every turn if a piece/ some pieces needs to be replaced in certain
     * conditions. Change them if so.
     */
    protected abstract void replacePieces();

    /**
     * Build the Tree in the sub-class.
     *
     * @param coord the coordinate of the piece.
     * @return the Tree of Step of the selected Piece.
     */
    protected abstract Tree<Step> calcTree(Coordinate coord);

    /**
     * Filter the map in the sub-class.
     *
     * @param treeMap the unfiltered Map of StepTree.
     * @return the filtered Map.
     */
    protected abstract Map<Coordinate, Tree<Step>> filterSteps(Map<Coordinate, Tree<Step>> treeMap);

    /**
     * PRIVATE METHODS.
     */

    /**
     * This method is called when a color ends its turn.
     *
     * @return the next color in the turn iteration.
     */
    private Color nextTurn() {
        final Color nextTurn = this.getTurnSequence().next();
        if (winCondition().isEmpty()) {
            calculateAllSteps();
            if (this.allStepTrees.isEmpty()) {
                this.killPlayer(getActualTurn());
                nextTurn();
            }
        }
        return nextTurn;
    }

    /**
     *
     * @param coord the coordinate of the piece on the Chessboard.
     * @return A tree of Steps, containing all the steps that the piece can do.
     */
    private Tree<Step> calculateTreeFromCoordinate(final Coordinate coord) {
        final Optional<Piece> pieceSelected = this.getLogicBoard().getBlock(coord).getPiece();
        if (!pieceSelected.isPresent()) {
            throw new IllegalArgumentException("Can't calculate StepTree for this coordinate:" + coord.toString());
        }
        return calcTree(coord);
    }

    /**
     * Kill the piece in the chosen coordinate. Add the piece to removedPieces
     * Map.This method is often used in combination with {@link #killPiece(Step)}.
     *
     * @param coord The coordinate where the piece to kill is.
     * @return true,if the piece existed, false if the piece was alredy killed in
     *         the previous step.
     */
    private boolean killPiece(final Coordinate coord) {
        if (getLogicBoard().getBlock(coord).pieceExists()) {
            final Piece killedPiece = getLogicBoard().getBlock(coord).getPiece().get();
            final Color deadPieceColor = killedPiece.getColor();
            // Add the piece to piecesRemoved.
            this.getPiecesRemoved().get(deadPieceColor).add(killedPiece);
            // Remove the piece from the board.
            getLogicBoard().getBlocks().get(coord).removePiece();
            // Check if the player is still alive.
            isPlayerStillAlive(deadPieceColor);
            return true;
        }
        return false;
    }

    /**
     * Kill the pieces contained in the step passed. This method is always used in
     * combination with {@link #killPiece(Coordinate)}.
     *
     * @param step The step containing the piece to kill
     */
    private boolean killPiece(final Step step) {
        if (step.getDeadPiece().isPresent()) {
            return killPiece(step.getDeadPiece().get().getX());
        }
        return false;
    }

    /**
     * Check if a player is still alive. If not, remove it from AlivePlayer.
     *
     * @param color
     */
    private boolean isPlayerStillAlive(final Color color) {
        if (getPiecesOnBoard().get(color).isEmpty()) {
            getAlivePlayers().remove(getPlayers().get(color));
            return false;
        }
        return true;
    }

    /**
     * Delete a player and his pieces from the game.
     *
     * @param deafColor the color of the player to kill.
     * @return the player killed.
     */
    private Player killPlayer(final Color deafColor) {
        if (getAlivePlayers().remove(getPlayers().get(deafColor))) {
            getLogicBoard().getBlocks().keySet().stream()
                    .filter(k -> getLogicBoard().getBlock(k).getPiece().isPresent())
                    .filter(k -> getLogicBoard().getBlock(k).getPiece().get().getColor().equals(deafColor))
                    .forEach(k -> {
                        // Add all the pieces to piecesRemoved.
                        getPiecesRemoved().get(deafColor).add(getLogicBoard().getBlock(k).getPiece().get());
                        // Remove the pieces from the board.
                        getLogicBoard().getBlock(k).removePiece();
                    });
            return getPlayers().get(deafColor);
        }
        throw new IllegalArgumentException(" The player is not present or is alredy dead!");
    }

    /*
     * /GETTERS AND SETTERS METHOD
     */

    /**
     * @return the history
     */
    protected History getHistory() {
        return history;
    }

    /**
     * @return the players
     */
    protected Map<Color, Player> getPlayers() {
        return players;
    }

    /**
     * @return the alivePlayers
     */
    protected Set<Player> getAlivePlayers() {
        return alivePlayers;
    }

    /**
     * @return the piecesOnBoard
     */
    protected Map<Color, List<Piece>> getPiecesOnBoard() {
        Map<Color, List<Piece>> piecesOnBoard;
        piecesOnBoard = getPlayers().keySet().stream().collect(Collectors.toMap(c -> c, c -> new ArrayList<>()));
        logicBoard.getBlocks().values().stream().filter(b -> b.getPiece().isPresent()).map(b -> b.getPiece().get())
                .forEach(p -> piecesOnBoard.get(p.getColor()).add(p));
        return piecesOnBoard;
    }

    /**
     * @return the piecesRemoved
     */
    protected Map<Color, List<Piece>> getPiecesRemoved() {
        return piecesRemoved;
    }

    /**
     * @return the turnSequence
     */
    protected TurnIterator getTurnSequence() {
        return turnSequence;
    }

    /**
     * @param status the updated status.
     */
    protected final void setStatus(final GameStatus status) {
        this.status = status;
    }

    /**
     * @return the move builder.
     */
    protected MoveBuilder getMoveBuilder() {
        return moveBuilder;
    }

    /**
     * {@inheritDoc}
     */
    protected void setMoveInProgress(final boolean moveInProgress) {
        this.moveInProgress = moveInProgress;
    }

}
