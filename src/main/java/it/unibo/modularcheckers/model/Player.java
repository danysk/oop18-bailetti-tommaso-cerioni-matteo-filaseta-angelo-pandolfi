package it.unibo.modularcheckers.model;

/**
 * The Players playing the game.
 */
public interface Player {

    /**
     * Get the name of the Player.
     * 
     * @return A string containing the name of the Player.
     */
    String getName();
}
