package it.unibo.modularcheckers.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import it.unibo.modularcheckers.model.move.Move;
import it.unibo.modularcheckers.util.LocalFilesUtils;

/**
 * Tracks all of the movements in the game.
 */
public class History implements Serializable {

    private static final long serialVersionUID = -5202543937802148522L;
    private final List<Move> actions;
    private int currentMove;

    /**
     * Constructs an empty list of Moves.
     */
    public History() {
        this.actions = new ArrayList<>();
        this.currentMove = 0;
    }

    /**
     * Returns the iterator for the moves list.
     *
     * @return Iterator of the list
     */
    public Iterator<Move> getIterator() {
        return this.actions.iterator();
    }

    /**
     * Returns the last move in the List.
     *
     * @return last element of the List
     */
    public Move getLastMove() {
        return getMove(this.currentMove);
    }

    /**
     * Returns the move in the current position.
     *
     * @param index index of the element to return
     * @return the element specified by the index
     */
    public Move getMove(final int index) {
        return this.actions.get(index);
    }

    /**
     * Sets the next move to the bottom of the List.
     *
     * @param move the move to be added to bottom
     */
    public void setMove(final Move move) {
        this.actions.add(move);
        this.currentMove++;
    }

    /**
     * Inserts the move in the index, destroying every Move done afterwards.
     *
     * @param move  move to be added to the list
     * @param index index of the list where the move should be inserted
     */
    public void setMove(final Move move, final int index) {
        this.actions.subList(index, this.currentMove).clear();
        setMove(move);
    }

    /**
     * Saves the current game to file.
     */
    public void saveToFile() {
        saveToFile(new Timestamp(System.currentTimeMillis()).toString());
    }

    /**
     * Saves the current game to file.
     *
     * @param name name of the file
     */
    private void saveToFile(final String name) {
        try {
            final FileOutputStream fileOutputStream = new FileOutputStream(
                    new File(LocalFilesUtils.getHistoryPath() + File.separator + name));
            final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(this);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the History from the File given.
     *
     * @param name String name of the file
     * @return History deserialized object from the file.
     */
    public History getFromFile(final String name) {
        History history = null;
        try {
            final FileInputStream fileInputStream = new FileInputStream(
                    new File(LocalFilesUtils.getHistoryPath() + File.separator + name));
            final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            history = (History) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return history;
    }

    /**
     * Deletes from list and returns the last move removed.
     *
     * @return the last move that has been removed
     * 
     *         public Move undoLastMove() { final Move m = new
     *         MoveImpl(this.actions.remove(this.actions.size() - 1).getSteps());
     *         this.currentMove--; return m; }
     */
}
